const URL = 'http://localhost:8080/api/user-profiles';

function userProfile(id, successCallback, failedCallback) {
    let info = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }    
    };
    let url = `${URL}/${id}`;

    fetch(url, info)
    .then(response => response.json())
    .then(result => {
        successCallback(result);
    }).catch(error => {
        failedCallback(error);
    });
}

export {
    userProfile,
};