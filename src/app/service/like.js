const URL = 'http://localhost:8080/api/like';

function like(id, successCallback, failedCallback) {
    let info = {
        method: 'POST',
        body: JSON.stringify({
            userProfileId: id,
            userName: 'anonymous user',
        }),
        headers: {
            'Content-Type': 'application/json'
        }    
    };

    fetch(URL, info)
    .then(result => {
        successCallback(result);
    }).catch(error => {
        failedCallback(error);
    });
}

export {
    like,
};