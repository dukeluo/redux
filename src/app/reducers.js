import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import profileReducer from './profile/reducers/profileReducer';

export default combineReducers({
  product: productReducer,
  user: profileReducer,
});