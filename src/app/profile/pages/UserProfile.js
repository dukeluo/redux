import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { bindActionCreators } from 'redux';
import {getLocalProfile, getProfile, likeUser} from '../actions/profileAction';
import { connect } from 'react-redux';
import './UserProfile.less';

class UserProfile extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const profiles = this.props.profile;
        const id = +this.props.match.params.id;
        const selected = profiles.find(item => item.id === id); 

        console.log("render", Date.now());
        
        return (
            <div className='user-profile'>
                <h3>User Profile</h3>
                <p>User Name: <span>{selected.name}</span></p>
                <p>Gender: <span>{selected.gender}</span></p>
                <p>Description: <span>{selected.description}</span></p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe natus adipisci repellat recusandae sint beatae voluptate earum omnis, animi, ullam explicabo soluta? Ratione iure deleniti consequatur! Tempore animi sint voluptatem!</p>
                <button onClick={() => this.props.likeUser(selected.id)} disabled={selected.liked ? true : false}>{selected.liked ? 'Liked' : 'Like'}</button>
                <Link to='/'>&lt; Back to home</Link>
            </div>
        );
    }

    // componentWillMount() {
    //     const id = +this.props.match.params.id;
    //     this.props.getProfile(id);
    //     console.log("will  ", Date.now());
    // }

    componentDidMount() {
        // this.props.getLocalProfile();
        const id = +this.props.match.params.id;

        console.log("did   ", Date.now());
        this.props.getProfile(id);
    }
}

const mapStateToProps = ({user}) => ({
    profile: user.profile
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    // getLocalProfile,
    getProfile: (id) => getProfile(id),
    likeUser: (id) => likeUser(id),
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);