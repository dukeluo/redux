import {userProfile} from '../../service/userProfile';
import {like} from '../../service/like';

const getLocalProfile = () => (dispatch) => {
    dispatch({
        type: 'GET_LOCAL_USER_PROFILE',
    });
};

const getProfile = (id) => (dispatch) => {
    const success = (data) => {
        dispatch({
            type: 'GET_USER_PROFILE',
            payload: {
                ...data,
                id
            },
        });
    };
    const failed = (data) => {
        console.error(data);
    };

    userProfile(id, success, failed);
};

const likeUser = (id) => (dispatch) => {
    const success = (data) => {
        dispatch({
            type: 'LIKE_USER',
            payload: {
                id: id,
                liked: true,
            },
        });
    };
    const failed = (data) => {
        console.error(data);
    };

    like(id, success, failed);
}

export {
    getLocalProfile,
    getProfile,
    likeUser,
};