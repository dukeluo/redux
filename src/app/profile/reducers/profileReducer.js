import profiles from '../profile.json';

const initState = {
    profile: profiles
};

const profileReducer = (state = initState, action) => {
    switch(action.type) {
        case 'GET_LOCAL_USER_PROFILE':
            return {
                ...state,
            };

        case 'GET_USER_PROFILE':
            let user = action.payload;
            let profile = state.profile.slice();
            let index = profile.findIndex(item => {
                return item.id === user.id;
            });

            if (index !== -1) {
                profile[index] = Object.assign(profile[index], user);
            } else {
                profile.push(user);
            }
            return {
                ...state,
                profile,
            };

        case 'LIKE_USER':
            user = action.payload;
            profile = state.profile;
            index = profile.findIndex(item => {
                return item.id === user.id;
            });

            profile[index].liked = user.liked;
            return {
                ...state
            };

        default:
            return state;
    }
};

export default profileReducer;